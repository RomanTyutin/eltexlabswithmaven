package com.eltex.lab_01;

public interface ICrudAction {
    void create();
    void read();
    void update();
    void delete();
}
