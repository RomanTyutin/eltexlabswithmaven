package com.eltex.lab_8.service;

import com.eltex.lab_8.entities.goods.Equipment;
import com.eltex.lab_8.repository.EquipmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EquipmentService {

    private final EquipmentRepository equipmentRepository;

    @Autowired
    public EquipmentService(EquipmentRepository equipmentRepository){
        this.equipmentRepository = equipmentRepository;
    }

    public List<Equipment> showAll(){
        return equipmentRepository.findAll();
    }

    public void add (Equipment equipment){
        equipmentRepository.save(equipment);
    }


}
