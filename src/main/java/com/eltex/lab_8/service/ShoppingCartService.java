package com.eltex.lab_8.service;

import com.eltex.lab_8.entities.ShoppingCart;
import com.eltex.lab_8.repository.ShoppingCartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShoppingCartService {

    private final ShoppingCartRepository shoppingCartRepository;

    @Autowired
    public ShoppingCartService(ShoppingCartRepository shoppingCartRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
    }

    public void save (ShoppingCart s){
        shoppingCartRepository.save(s);
    }

    public List<ShoppingCart> showAll (){
        return shoppingCartRepository.findAll();
    }
}
