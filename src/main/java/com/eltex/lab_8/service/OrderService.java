package com.eltex.lab_8.service;

import com.eltex.lab_8.entities.Credentials;
import com.eltex.lab_8.entities.Order;
import com.eltex.lab_8.entities.ShoppingCart;
import com.eltex.lab_8.entities.goods.Equipment;
import com.eltex.lab_8.entities.goods.SetTopBox;
import com.eltex.lab_8.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class OrderService {

    private final OrderRepository orderRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public List<Order> showAll(){
        return orderRepository.findAll();
    }


    public Order showById(UUID id){
        return orderRepository.findById(id).orElse(null);
    }


    public void delById(UUID id){
        orderRepository.deleteById(id);
    }


    public boolean addById (UUID id){
        if(orderRepository.findById(id).orElse(null) != null){
            return false;
        }

        //generating order
        Equipment e = new SetTopBox();
        e.create();
        ShoppingCart sc = new ShoppingCart();
        e.setShoppingCart(sc);
        sc.add(e);

        Credentials cr = new Credentials();
        cr.generateRandomCustomer();

        Order order = new Order();
        order.setCart(sc);
        order.setCredentials(cr);
        order.setId(id);

        //saving order to database
        orderRepository.save(order);
        return true;
    }

}
