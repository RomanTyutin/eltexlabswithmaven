package com.eltex.lab_8;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application8 {
    private static Logger logger = LogManager.getLogger(Application8.class.getName());

    public static void main(String[] args) {
        SpringApplication.run(Application8.class, args);

        logger.info("Application start.");
    }

}
