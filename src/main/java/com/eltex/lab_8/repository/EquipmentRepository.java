package com.eltex.lab_8.repository;

import com.eltex.lab_8.entities.goods.Equipment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface EquipmentRepository extends JpaRepository<Equipment, UUID> {
}
