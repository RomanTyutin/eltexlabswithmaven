package com.eltex.lab_8.repository;

import com.eltex.lab_8.entities.Credentials;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CredentialsRepository extends JpaRepository<Credentials, UUID> {
}
