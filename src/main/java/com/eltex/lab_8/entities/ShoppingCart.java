package com.eltex.lab_8.entities;

import com.eltex.lab_8.entities.goods.Equipment;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Entity
public class ShoppingCart implements Serializable {

    @Id
    @Column(name = "id", columnDefinition = "BINARY(16)")
    private UUID id;

    @OneToMany(orphanRemoval = true,
            mappedBy = "shoppingCart",cascade = CascadeType.ALL)
    @Fetch(FetchMode.SUBSELECT)
    private List<Equipment> shoppingList;

    public ShoppingCart() {
        this.id = UUID.randomUUID();
        this.shoppingList = new LinkedList<>();
    }

    public List<Equipment> getShoppingList() {
        return shoppingList;
    }

    public void setShoppingList(List<Equipment> shoppingList) {
        this.shoppingList = shoppingList;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void add (Equipment equpment){
        shoppingList.add(equpment);
    }


}
