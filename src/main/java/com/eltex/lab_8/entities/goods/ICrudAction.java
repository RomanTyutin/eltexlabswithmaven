package com.eltex.lab_8.entities.goods;

public interface ICrudAction {
    void create();
    void read();
    void update();
    void delete();
}
