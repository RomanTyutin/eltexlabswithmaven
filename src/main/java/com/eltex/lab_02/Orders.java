package com.eltex.lab_02;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Iterator;
import java.util.PriorityQueue;

public class Orders {

    private PriorityQueue<Order> ordersQueue = new PriorityQueue<>();
    private HashMap<LocalDateTime, Order> ordersTimeSet = new HashMap<>();

    public void makeBuy(Credentials c, ShoppingCart s) {

        Order order = new Order(c, s);

        ordersQueue.add(order);
        ordersTimeSet.put(order.getCreationMoment(), order);

    }

    public void checkOrders() {

        Iterator<Entry<LocalDateTime, Order>> iter = ordersTimeSet.entrySet().iterator();
        while(iter.hasNext()){
            Entry<LocalDateTime, Order> entry =  iter.next();

            if (entry.getKey().compareTo(LocalDateTime.now().minusSeconds(Order.getWaitingTime())) < 0
                    || "Order has been proccesed.".equals(entry.getValue().getStatus())) {
                ordersQueue.remove(entry.getValue());
                iter.remove();
            }

        }


/*        ordersTimeSet.forEach((k, v) ->
        {
            if (k.compareTo(LocalDateTime.now().minusSeconds(Order.getWaitingTime())) < 0
            || "Order has been proccesed.".equals(v.getStatus())) {
                ordersQueue.remove(v);
                ordersTimeSet.remove(k);
            }
        });*/

    }


    public void showAll (){

        ordersTimeSet.forEach((k, v) -> v.showInfo());
        if(ordersTimeSet.isEmpty()){
            System.out.println("There are no orders!");
        }

    }

}
