package com.eltex.lab_04;
import java.util.Scanner;
import java.util.UUID;
import java.util.Random;

/**
 * This class describes the customer
 */

public class Credentials {

    private UUID customersId;
    private String customersName;
    private String customersPatronimic;
    private String customersSurname;
    private String customersEmail;


    private Random random = new Random();
    private Scanner scanner = new Scanner(System.in);

    public Credentials (){
        customersId = UUID.randomUUID();
    }

    public UUID getCustomersId() {
        return customersId;
    }

    public void setCustomersId(UUID customersId) {
        this.customersId = customersId;
    }

    public String getCustomersName() {
        return customersName;
    }

    public void setCustomersName(String customersName) {
        this.customersName = customersName;
    }

    public String getCustomersPatronimic() {
        return customersPatronimic;
    }

    public void setCustomersPatronimic(String customersPatronimic) {
        this.customersPatronimic = customersPatronimic;
    }

    public String getCustomersSurname() {
        return customersSurname;
    }

    public void setCustomersSurname(String customersSurname) {
        this.customersSurname = customersSurname;
    }

    public String getCustomersEmail() {
        return customersEmail;
    }

    public void setCustomersEmail(String customersEmail) {
        this.customersEmail = customersEmail;
    }

    public void setCustomersData(){

        System.out.println("enter your name");
        do {
            customersName = scanner.nextLine();
        } while (customersName.isEmpty());

        System.out.println("enter your surname");
        do {
            customersSurname = scanner.nextLine();
        } while (customersSurname.isEmpty());

        System.out.println("enter your father's name");
        do {
            customersPatronimic = scanner.nextLine();
        } while (customersPatronimic.isEmpty());

        System.out.println("enter your email");
        do {
            customersEmail = scanner.nextLine();
        } while (customersEmail.isEmpty());
    }


    public void setRandomCustomersData(){
        int forRandom = random.nextInt(2);
        switch(forRandom){
            case 0:
                customersName = "Ivan";
                customersSurname = "Romanov";
                customersPatronimic = "Alexeyevich";
                customersEmail = "prostoczar@mail.ru";
                break;
            case 1:
                customersName = "Sergey";
                customersSurname = "Lebedev";
                customersPatronimic = "Sergeyevich";
                customersEmail = "sls@gmail.com";
                break;
        }
    }


    public String toString(){
        return "Customer's Name: " + customersName + "  " + customersPatronimic +
                "  " + customersSurname + "    Customer's email: " + customersEmail +
                "    Customer's Id: " + customersId;
    }

}
