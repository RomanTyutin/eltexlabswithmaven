package com.eltex.lab_04;

public interface ICrudAction {
    void create();
    void read();
    void update();
    void delete();
}
