package com.eltex.lab_06;

public class AwatingCheck extends ACheck {

    public AwatingCheck(Orders orders, int numberOfChecks, long time){
        super(orders, numberOfChecks, time);
    }

    @Override
    public void run() {
        for (int i = 0 ; i < numberOfChecks ; i++) {
            try {
                Thread.sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            orders.changeOrdersStatus();
            System.out.println("Orders' status has been changed.");
            System.out.println(" ");
        }
    }
}
