package com.eltex.lab_06;

import java.time.LocalDateTime;
import java.util.*;
import java.util.Map.Entry;

public class Orders<T extends Order> {



    private PriorityQueue<T> ordersQueue = new PriorityQueue<>();
    private HashMap<LocalDateTime, T> ordersTimeSet = new HashMap<>();

    public synchronized PriorityQueue<T> getOrdersQueue() {
        return ordersQueue;
    }

    public synchronized void makeBuy(T t) {

        ordersTimeSet.put(t.getCreationMoment(), t);
        ordersQueue.add(t);

    }

    public synchronized void changeOrdersStatus() {

        Iterator<Entry<LocalDateTime, T>> iter = ordersTimeSet.entrySet().iterator();
        while (iter.hasNext()) {

            Entry<LocalDateTime, T> entry = iter.next();

            if ("Awating".equals(entry.getValue().getStatus())) {
                entry.getValue().hasBeenProcessed();
            }
        }
    }


    public synchronized void checkOrders() {

        Iterator<Entry<LocalDateTime, T>> iter = ordersTimeSet.entrySet().iterator();
        while (iter.hasNext()) {
            Entry<LocalDateTime, T> entry = iter.next();

            if (/*entry.getKey().compareTo(LocalDateTime.now().minusSeconds(Order.getWaitingTime())) < 0
                    ||*/ "Order has been processed.".equals(entry.getValue().getStatus())) {
                ordersQueue.remove(entry.getValue());
                iter.remove();
            }

        }


/*        ordersTimeSet.forEach((k, v) ->
        {
            if (k.compareTo(LocalDateTime.now().minusSeconds(Order.getWaitingTime())) < 0
            || "Order has been processed.".equals(v.getStatus())) {
                ordersQueue.remove(v);
                ordersTimeSet.remove(k);
            }
        });*/

    }


    public synchronized void showAll() {

        ordersTimeSet.forEach((k, v) -> v.showInfo());
        if (ordersTimeSet.isEmpty()) {
            System.out.println("There are no orders!");
        }
        System.out.println("Map's size: " + ordersTimeSet.size());
        System.out.println("Queue's size: " + ordersQueue.size());
        System.out.println(" ");
    }


    /**
     * @param id id of desired order
     * @return order with id or null if there are no desired order
     */
    public synchronized T showById(UUID id){
        T order = null;
        Iterator<Entry<LocalDateTime, T>> iter = ordersTimeSet.entrySet().iterator();
        while (iter.hasNext()) {

            Entry<LocalDateTime, T> entry = iter.next();

            if (id.equals(entry.getValue().getCustomer().getCustomersId())) {
                order = entry.getValue();
            }
        }
        return order;
    }


    public synchronized int deleteOrderById(UUID id) {

        Iterator<Entry<LocalDateTime, T>> iter = ordersTimeSet.entrySet().iterator();
        while (iter.hasNext()) {
            Entry<LocalDateTime, T> entry = iter.next();

            if (id.equals(entry.getValue().getCustomer().getCustomersId())) {
                ordersQueue.remove(entry.getValue());
                iter.remove();
                return 0;
            }

        }
        return 1;
    }

    public synchronized Order createRandomOrder(UUID id){

        //check orders with same id
        if(showById(id) != null){
            return null;
        }


        Equipment equipment = null;
        Random random = new Random();
        int type = random.nextInt(3);
        if (type == 0) {
            equipment = new TV();
        } else if (type == 1) {
            equipment = new SetTopBox();
        } else {
            equipment = new Console();
        }

        equipment.create();

        ShoppingCart cart = new ShoppingCart();
        cart.add(equipment);

        Credentials customer = new Credentials(id);
        customer.setRandomCustomersData();

        Order order = new Order(customer, cart);
        return order;
    }

}
