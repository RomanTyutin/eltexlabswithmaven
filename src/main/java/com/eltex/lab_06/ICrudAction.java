package com.eltex.lab_06;

public interface ICrudAction {
    void create();
    void read();
    void update();
    void delete();
}
