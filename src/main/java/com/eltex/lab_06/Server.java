package com.eltex.lab_06;

import java.io.IOException;
import java.net.*;
import java.util.Map;

public class Server {


    public void sendTCPPort(int portTCP, int portUDP){

        try(DatagramSocket datagramSocket = new DatagramSocket()){
            datagramSocket.setBroadcast(true);
            byte[] portBuffer = Integer.toString(portTCP).getBytes();
            DatagramPacket datagramPacket = new DatagramPacket(portBuffer, portBuffer.length,
                    InetAddress.getByName("255.255.255.255"), portUDP);
            datagramSocket.send(datagramPacket);
        }catch (SocketException e){
            e.printStackTrace();
        }catch (UnknownHostException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }

    }

    public void obtaineOrder(int portTCP, Map<InetAddress, Order> map){
        try(ServerSocket serverSocket = new ServerSocket(portTCP)){
            while(true){
                Socket incoming = serverSocket.accept();
                Runnable r = new TCPThread(incoming, map);
                Thread thread = new Thread(r);
                thread.start();
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }


}
