package com.eltex.lab_06;

public abstract class ACheck extends Thread{

    protected Orders orders;
    protected long time;
    protected int numberOfChecks;

    public ACheck(Orders orders, int numberOfChecks, long time){
        this.orders = orders;
        this.time = time;
        this.numberOfChecks = numberOfChecks;
    }

}
