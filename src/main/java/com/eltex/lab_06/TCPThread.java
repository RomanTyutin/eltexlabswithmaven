package com.eltex.lab_06;



import java.io.*;
import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.time.LocalDateTime;
import java.util.Map;

public class TCPThread implements Runnable {

    Socket socket;
    Map<InetAddress, Order> map;

    public TCPThread(Socket socket, Map<InetAddress, Order> map){
        this.socket = socket;
        this.map = map;
    }



    @Override
    public void run() {
        try(InputStream in = socket.getInputStream()){
            ObjectInputStream ois = new ObjectInputStream(in);
            Order order = (Order) ois.readObject();
            order.hasBeenProcessed();
            order.showInfo();
            InetAddress address = socket.getInetAddress();
            map.put(address, order);
            sendUDPNotification(address, order);
        }catch (IOException e){
            e.printStackTrace();
        }catch (ClassNotFoundException e){
            System.out.println("Class of a serialized order cannot be found!");
        }
    }


    public void sendUDPNotification(InetAddress iP, Order order){
        ProcessedOrder processedOrder =
                new ProcessedOrder(order, LocalDateTime.now());
        try(DatagramSocket notification = new DatagramSocket();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);){
            oos.writeObject(processedOrder);
            oos.flush();
            byte[] processedOrderBytes = baos.toByteArray();
            DatagramPacket datagramPacket2 = new DatagramPacket(
                    processedOrderBytes, processedOrderBytes.length,
                    iP, 4500);
            notification.send(datagramPacket2);
        }catch (SocketException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
