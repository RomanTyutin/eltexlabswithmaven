package com.eltex.lab_06;

import java.util.PriorityQueue;
import java.util.UUID;

public interface IOrder {
    Order readById(UUID id, String fileName);
    void saveById(UUID id, String fileName);
    PriorityQueue readAll(String fileName);
    void saveAll(String fileName);
}
