package com.eltex.lab_06;

import java.io.*;
import java.net.*;
import java.util.Random;

public class Client {

    public InetSocketAddress obtaineTCPPort(int portUDP){
        InetSocketAddress isa = null;

        try(DatagramSocket datagramSocket = new DatagramSocket(portUDP)){
            DatagramPacket datagramPacket = new DatagramPacket(new byte[256], 256);
            datagramSocket.receive(datagramPacket);
            int port = Integer.parseInt(new String(datagramPacket.getData(),
                    0, datagramPacket.getLength()));
            InetAddress address = datagramPacket.getAddress();
            isa = new InetSocketAddress(address, port);
        }catch (SocketException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }

        return isa;
    }

    public void sendOrder (InetSocketAddress isa){

        try(Socket socket = new Socket(isa.getAddress(), isa.getPort());
            OutputStream out = socket.getOutputStream()){
            ObjectOutputStream oos = new ObjectOutputStream(out);
            Order order = CreateRandomOrder();
            oos.writeObject(order);
            oos.close();
        }catch(IOException e){
            e.printStackTrace();
        }


    }

    public void obtaineUDPNotification(InetSocketAddress isa){

        while(true){

            try(DatagramSocket datagramSocket = new DatagramSocket(4500)){
                DatagramPacket datagramPacket = new DatagramPacket(new byte[2048], 2048);
                datagramSocket.receive(datagramPacket);
                ByteArrayInputStream bais = new ByteArrayInputStream(datagramPacket.getData());
                ObjectInputStream ois = new ObjectInputStream(bais);
                ProcessedOrder processedOrder = (ProcessedOrder) ois.readObject();
                processedOrder.showNotificationInfo();
            }catch (SocketException e){
                e.printStackTrace();
            }catch (IOException e){
                e.printStackTrace();
            }catch (ClassNotFoundException e){
                e.printStackTrace();
            }

            sendOrder(isa);

        }


    }

    public Order CreateRandomOrder(){
        Equipment equipment = null;
        Random random = new Random();
        int type = random.nextInt(3);
        if (type == 0) {
            equipment = new TV();
        } else if (type == 1) {
            equipment = new SetTopBox();
        } else {
            equipment = new Console();
        }

        equipment.create();

        ShoppingCart cart = new ShoppingCart();
        cart.add(equipment);

        Credentials customer = new Credentials();
        customer.setRandomCustomersData();

        Order order = new Order(customer, cart);
        return order;
    }
}
