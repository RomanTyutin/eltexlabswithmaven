package com.eltex.lab_06;

import java.io.Serializable;
import java.time.LocalDateTime;

public class ProcessedOrder implements Serializable {

    private Order order;
    private LocalDateTime processingTime;

    public ProcessedOrder(Order order, LocalDateTime processingTime){
        this.order = order;
        this.processingTime = processingTime;
    }


    public void showNotificationInfo(){
        System.out.println("Order has been processed at " + processingTime);
        System.out.println("Order's information: ");
        order.showInfo();
    }

}
