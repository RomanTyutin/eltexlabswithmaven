package com.eltex.lab_06;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

//@JsonTypeName("tV")
public class TV extends Equipment {

    private int diagonal_size;                  // variables for object's parametr
    //private Random random = new Random();

    public int getDiagonal_size() {
        return diagonal_size;
    }

    public void setDiagonal_size(int diagonal_size) {
        this.diagonal_size = diagonal_size;
    }


    public TV() {
        super();
        diagonal_size = 0;

    }

    public TV(int diagonal_size) {
        this.diagonal_size = diagonal_size;
    }

    public void create() {
        super.create();
        Random random = new Random();
        diagonal_size = random.nextInt(200);
    }

    public void read() {
        super.read();
        System.out.println("Diagonal size: " + diagonal_size);
    }

    public void update() {
        super.update();

        boolean diagonal_input;
        do {
            diagonal_input = false;
            try {
                System.out.println("enter diagonal size of TV");
                Scanner scanner = new Scanner(System.in);
                diagonal_size = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Enter price as a integer, please!");
                diagonal_input = true;
            }
        } while (diagonal_input);
    }

    @Override
    public void delete() {
        super.delete();
        diagonal_size = 0;
    }

    public String toString (){
        return super.toString() + "    Diagonal size: " + diagonal_size;
    }

}

