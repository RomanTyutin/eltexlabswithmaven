package com.eltex.lab_7;

public class WrongCommandException extends Exception {

    public WrongCommandException(){};

    public WrongCommandException(String massage){
        super(massage);
    }
}
