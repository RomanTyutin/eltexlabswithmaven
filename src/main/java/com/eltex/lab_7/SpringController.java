package com.eltex.lab_7;

import com.eltex.lab_06.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;



@RestController
public class SpringController {


    private static final Logger logger = LogManager.getLogger(SpringController.class.getName());
    private Orders orders = new Orders();

//    @RequestMapping(value="/**")
//    public ResponseEntity wrongRequest(){
//
//
//        Map<String, String> commands = new HashMap<String, String>(){{
//            put("!", "Wrong Request!");
//            put("1","Enter \"?command=readAll\" to see list of orders");
//            put("2","Enter \"?command=readById&id=[id]\" to see order with desired id");
//            put("3","Enter \"?command=addToCard&id=[id]\" to add order with desired id");
//            put("4","Enter \"?command=delById&id=[id]\" to delete order with desired id");
//        }};
//
//        return new ResponseEntity(commands, HttpStatus.NOT_FOUND);
//    }




    @RequestMapping(value = "/", params = {"command"})
    public ResponseEntity readAll(@RequestParam(value="command") String command)
    throws WrongCommandException{
        if ("readall".equalsIgnoreCase(command)){
            logger.info("Client has obtained list of orders.");
            return new ResponseEntity(orders.getOrdersQueue(), HttpStatus.OK);
        }else{
            logger.warn("Wrong command.");
            throw new WrongCommandException();
            //return new ResponseEntity("Wrong Command!!!", HttpStatus.NOT_FOUND);
        }

    }


    @RequestMapping(value = "/", params = {"command", "id"})
    public ResponseEntity doSmthWithId(@RequestParam(value="command") String command,
                                       @RequestParam(value = "id") String id)
    throws WrongCommandException{


        UUID uuid = null;

            uuid = UUID.fromString(id);
            logger.info("Id was entered correctly.");


        if("addToCard".equalsIgnoreCase(command)){
            orders.makeBuy(orders.createRandomOrder(uuid));
            logger.info("New order was added.");
            return new ResponseEntity("Order has been added. Order's id: " + uuid.toString(),
                    HttpStatus.OK);
        }else if("readById".equalsIgnoreCase(command)){
            logger.info("try to read order by id");
            if(orders.showById(uuid) == null){
                logger.info("order with id-" + uuid.toString() + "isn't found.");
                return new ResponseEntity("There are no order with id: " + uuid.toString(),
                        HttpStatus.NOT_FOUND);
            }else{
                logger.info("order with id-" + uuid.toString() + "is found.");
                return new ResponseEntity(orders.showById(uuid), HttpStatus.OK);
            }
        }else if("delById".equalsIgnoreCase(command)){
            int isDeleted = orders.deleteOrderById(uuid);
            logger.info("try to delete order by id");
            if(isDeleted == 0){
                logger.info("order with id-" + uuid.toString() + "is deleted.");
                return new ResponseEntity(isDeleted, HttpStatus.OK);
            }else{
                logger.warn("order with id-" + uuid.toString() + "isn't found." +
                        "and couldn't be deleted");
                return new ResponseEntity(isDeleted, HttpStatus.NOT_FOUND);
            }
        }else{
            logger.warn("Wrong command with id.");
            throw new WrongCommandException();
            //return new ResponseEntity("Wrong Command!!!", HttpStatus.NOT_FOUND);
        }
    }
}
