package com.eltex.lab_05;

public interface ICrudAction {
    void create();
    void read();
    void update();
    void delete();
}
