package com.eltex.lab_05;

import java.io.*;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.UUID;

public class ManagerOrderFile extends AManageOrder {

    public ManagerOrderFile(Orders orders) {
        super(orders);
    }

    @Override
    public Order readById(UUID id, String fileName) {

        Order deciredOrder = null;
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName))) {
            while(true){
                try {
                    Order readedOrder = (Order) ois.readObject();
                    if(readedOrder.getCustomer().getCustomersId().equals(id)){
                        deciredOrder = readedOrder;
                        break;
                    }
                }catch (EOFException e){
                    break;
                }
            }
        } catch (IOException e) {
            System.out.println("File isn't found or file's writing problem!");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            //System.out.println("File isn't found or file's writing problem!");
            e.printStackTrace();
        }

            return deciredOrder;

    }

    @Override
    public void saveById(UUID id, String fileName) {

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
            PriorityQueue<Order> queue = orders.getOrdersQueue();
            Iterator<Order> iter= queue.iterator();
            Boolean orderIsFoundById = false;
            while(iter.hasNext()){
                Order deciredOrder = iter.next();
                if(deciredOrder.getCustomer().getCustomersId().equals(id)) {
                    oos.writeObject(deciredOrder);
                    orderIsFoundById = true;
                }
            }
            if(!orderIsFoundById){
                System.out.println("Order isn't found");
            }
        } catch (IOException e) {
            System.out.println("File isn't found or file's writing problem!");
            e.printStackTrace();
        }

    }

    @Override
    public PriorityQueue readAll(String fileName) {

        PriorityQueue<Order> queue = new PriorityQueue<>();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName))) {
            while(true){
                try {
                    Order readedOrder = (Order) ois.readObject();
                    queue.add(readedOrder);
                }catch (EOFException e){
                    break;
                }
            }
        } catch (IOException e) {
            System.out.println("File isn't found or file's writing problem!");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            //System.out.println("File isn't found or file's writing problem!");
            e.printStackTrace();
        }
        return queue;

    }



    @Override
    public void saveAll(String fileName) {

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
            PriorityQueue<Order> queue = orders.getOrdersQueue();
            Iterator<Order> iter= queue.iterator();
            while(iter.hasNext()){
                oos.writeObject(iter.next());
            }
        } catch (IOException e) {
            System.out.println("File isn't found or file's writing problem!");
            e.printStackTrace();
        }

    }
}
