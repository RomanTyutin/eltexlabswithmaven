package com.eltex.lab_05;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.UUID;
import java.io.File;

public class ManagerOrderJSON extends AManageOrder {

    public ManagerOrderJSON(Orders orders) {
        super(orders);
    }

    @Override
    public Order readById(UUID id, String fileName) {
        Order order = new Order();

        try {
            File file = new File(fileName);
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            Order[] ordersArray = mapper.readValue(file, Order[].class);

            boolean isFoundById = false;
            for(Order o: ordersArray){
                if(o.getCustomer().getCustomersId().equals(id)){
                    order = o;
                    isFoundById = true;
                }
            }

            if (!isFoundById){
                System.out.println("Order isn't found");
            }

        }catch (IOException e){
            e.printStackTrace();
        }

        return order;
    }

    @Override
    public void saveById(UUID id, String fileName) {

        try {
            PriorityQueue<Order> queue = orders.getOrdersQueue();
            Iterator<Order> iter= queue.iterator();
            boolean idIsContaned = false;
            while(iter.hasNext()) {
                Order deciredOrder = iter.next();
                if(deciredOrder.getCustomer().getCustomersId().equals(id)){
                    Order[] array = {deciredOrder};
                    File file = new File(fileName);
                    ObjectMapper mapper = new ObjectMapper();
                    mapper.registerModule(new JavaTimeModule());
                    mapper.writeValue(file, array);
                    idIsContaned = true;
                }
            }

            if (!idIsContaned){
                System.out.println("Order isn't found");
            }

        }catch (IOException e){
            e.printStackTrace();
        }

    }

    @Override
    public PriorityQueue readAll(String fileName) {
        PriorityQueue<Order> queue = new PriorityQueue<>();

        try {
            File file = new File(fileName);
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            Order[] ordersArray = mapper.readValue(file, Order[].class);


            for(Order o: ordersArray){
                queue.add(o);
            }
        }catch (IOException e){
            e.printStackTrace();
        }

        return queue;

    }

    @Override
    public void saveAll(String fileName) {

        try {
            PriorityQueue<Order> queue = orders.getOrdersQueue();
            File file = new File(fileName);
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            mapper.writeValue(file, orders.getOrdersQueue());
            //System.out.println(mapper.writeValueAsString(queue));
        }catch (IOException e){
            e.printStackTrace();
        }

    }
}
