package com.eltex.lab_05;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.UUID;

public class Main {

    public static void main(String[] args) throws JsonProcessingException {

        Orders orders = new Orders();


        new OrderMaker(orders, 2, 0).start();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        orders.showAll();

        //          For demonstrating ManagerOrderFile class

        ManagerOrderFile managerOrderFile = new ManagerOrderFile(orders);
        managerOrderFile.saveAll("filezzz");


        System.out.println("Queue's size" + orders.getOrdersQueue().size());


        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter id of order to be saved");
        UUID id = UUID.fromString(scanner.nextLine());
        managerOrderFile.saveById(id, "filezzz222");

        System.out.println("From file by readAll void");
        PriorityQueue<Order> queue = managerOrderFile.readAll("filezzz");
        for (Order o: queue){
            o.showInfo();
        }


        Order deciredOrder = managerOrderFile.readById(id, "filezzz222");
        System.out.println(" ");
        System.out.println("From filezzz222 by readById void:");
        deciredOrder.showInfo();


//          For demonstrating ManagerOrderJSON class

//        ManagerOrderJSON jsonner = new ManagerOrderJSON(orders);
//
//        jsonner.saveAll("file.json");
//
//        System.out.println("From file by readAll void");
//        PriorityQueue<Order> queue = jsonner.readAll("file.json");
//        for (Order o: queue){
//            o.showInfo();
//        }
//
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Enter id of order to be saved");
//        UUID id = UUID.fromString(scanner.nextLine());
//        jsonner.saveById(id, "file_id.json");
//
//        Order deciredOrderJSON = jsonner.readById(id, "file_id.json");
//        System.out.println(" ");
//        System.out.println("From file_id.json by readById void:");
//        deciredOrderJSON.showInfo();




    }

}
