package com.eltex.lab_03;

import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {

        try {

            try {


                int quantity = Integer.parseInt(args[0]);
                System.out.println("Quantity of new equipment is " + quantity);
                if ("TV".equalsIgnoreCase(args[1]) || "Console".equalsIgnoreCase(args[1]) || "Box".equalsIgnoreCase(args[1])) {
                    System.out.println("Type of new equipment is " + args[1]);
                } else {
                    System.out.println("Incorrect value of type!!! (Correct types are TV/Console/Box)");
                    System.exit(0);
                }

                ShoppingCart cart = new ShoppingCart();


                int i = 0;
                while (i < quantity) {


                    Equipment object = null;


                    if ("TV".equalsIgnoreCase(args[1])) {
                        object = new TV();
                    } else if ("Console".equalsIgnoreCase(args[1])) {
                        object = new Console();
                    } else if ("Box".equalsIgnoreCase(args[1])) {
                        object = new SetTopBox();
                    }


                    object.create();
//                    object.read();
//                    object.update();
//                    object.delete();
//                    System.out.println("Equipment's data has been deleted");
//                    object.create();


                    cart.add(object); // The equipment has been added into ShoppingCart

                    i++;
                }

                cart.showAll(); // demonstration of equipment into ShoppingCart

                Scanner scanner = new Scanner(System.in);

                // searching equipment by id into ShoppingCart
                try {
                    System.out.print("enter Id for searching equipment: ");
                    //UUID idforsearcing = UUID.fromString(scanner.nextLine());
                    cart.findById(UUID.fromString(scanner.nextLine()));
                } catch (IllegalArgumentException e) {
                    System.out.println("Id has been written incorrect!");
                }

                // obtaining customer's data from keyboard
                Credentials customer = new Credentials();
                customer.setCustomersData();

                // customer's and equipment's data were put into Orders' collections
                Order order = new Order(customer, cart);
                Orders orders = new Orders();
                orders.makeBuy(order);
                orders.showAll();


                // check if order was deleted, by using .checkOrders() void after end of
                // wating time
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {

                    @Override
                    public void run() {
                        orders.checkOrders();
                        System.out.println("After checking: ");
                        orders.showAll();
                        System.out.println("Ok");
                        timer.cancel();
                    }

                }, 6000);



            } catch (NumberFormatException e) {
                System.out.println("First argument should be integer!");
            }

        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("To start this program you should enter quantity of goods and type of goods " +
                    "(TV/Console/Box)");
        }
    }

}
