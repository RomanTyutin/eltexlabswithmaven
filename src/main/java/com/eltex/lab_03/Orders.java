package com.eltex.lab_03;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.PriorityQueue;

public class Orders <T extends Order>{

    private PriorityQueue<T> ordersQueue = new PriorityQueue<>();
    private HashMap<LocalDateTime, T> ordersTimeSet = new HashMap<>();

    public void makeBuy(T t) {

        ordersQueue.add(t);
        ordersTimeSet.put(t.getCreationMoment(), t);

    }

    public void checkOrders() {

        Iterator<Entry<LocalDateTime, T>> iter = ordersTimeSet.entrySet().iterator();
        while(iter.hasNext()){
            Entry<LocalDateTime, T> entry =  iter.next();

            if (entry.getKey().compareTo(LocalDateTime.now().minusSeconds(Order.getWaitingTime())) < 0
                    || "Order has been proccesed.".equals(entry.getValue().getStatus())) {
                ordersQueue.remove(entry.getValue());
                iter.remove();
            }

        }


/*        ordersTimeSet.forEach((k, v) ->
        {
            if (k.compareTo(LocalDateTime.now().minusSeconds(Order.getWaitingTime())) < 0
            || "Order has been proccesed.".equals(v.getStatus())) {
                ordersQueue.remove(v);
                ordersTimeSet.remove(k);
            }
        });*/

    }


    public void showAll (){

        ordersTimeSet.forEach((k, v) -> v.showInfo());
        if(ordersTimeSet.isEmpty()){
            System.out.println("There are no orders!");
        }

    }

}
